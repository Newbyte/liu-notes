# LiU Notes

HTML output hosted by GitLab at https://newbyte.gitlab.io/liu-notes/

This is a collection of Linköpings universitet-related notes to offload my
memory. Currently only contains a list of their online services and a
description of what each one is for.
